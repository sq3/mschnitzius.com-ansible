
mschnitzius.com Network Ansibel Repository
=============================================

This repository manages the inventory of the mschnitzius.com network.

	roles
	├── arch-common
	├── docker
	├── letsencrypt-docker
	├── mysql-docker
	├── nextcloud-docker
	├── nginx-docker
	├── owncloud-docker
	└── postgresql-docker
	playbooks
	├── arch
	└── nextcloud-docker
